# Guile SRFI 145

[SRFI 145](https://srfi.schemers.org/srfi-145/) ported to Guile

# INSTALL

`guix install guile-srfi-145`

# LICENSE

GPL-3
