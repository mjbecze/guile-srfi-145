(library (srfi srfi-145)
  (export assume)
  (import (guile))
  (begin
    (cond-expand-provide (current-module) '(srfi-145))
    (define-syntax assume
      (syntax-rules ()
        ((_ expression message ...)
         (or expression
             (error "invalid assumption" (quote expression) (list message ...))))
        ((_ . _)
         (syntax-error "invalid assume syntax"))))))